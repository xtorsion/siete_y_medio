#include "cards.h"
#include <cstdlib>
#include <iostream>

/*
You might or might not need these two extra libraries
#include <iomanip>
#include <algorithm>
*/


/* *************************************************
Card class
************************************************* */

/*
Default constructor for the Card class.
It could give repeated cards. This is OK.
Most variations of Blackjack are played with
several decks of cards at the same time.
*/
Card::Card() {
	int r = 1 + rand() % 4;
	switch (r) {
	case 1: suit = OROS; break;
	case 2: suit = COPAS; break;
	case 3: suit = ESPADAS; break;
	case 4: suit = BASTOS; break;
	default: break;
	}

	r = 1 + rand() % 10;
	switch (r) {
	case  1: rank = AS; break;
	case  2: rank = DOS; break;
	case  3: rank = TRES; break;
	case  4: rank = CUATRO; break;
	case  5: rank = CINCO; break;
	case  6: rank = SEIS; break;
	case  7: rank = SIETE; break;
	case  8: rank = SOTA; break;
	case  9: rank = CABALLO; break;
	case 10: rank = REY; break;
	default: break;
	}
}

// Accessor: returns a string with the suit of the card in Spanish 
string Card::get_spanish_suit() const {
	string suitName;
	switch (suit) {
	case OROS:
		suitName = "oros";
		break;
	case COPAS:
		suitName = "copas";
		break;
	case ESPADAS:
		suitName = "espadas";
		break;
	case BASTOS:
		suitName = "bastos";
		break;
	default: break;
	}
	return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish 
string Card::get_spanish_rank() const {
	string rankName;
	switch (rank) {
	case AS:
		rankName = "As";
		break;
	case DOS:
		rankName = "Dos";
		break;
	case TRES:
		rankName = "Tres";
		break;
	case CUATRO:
		rankName = "Cuatro";
		break;
	case CINCO:
		rankName = "Cinco";
		break;
	case SEIS:
		rankName = "Seis";
		break;
	case SIETE:
		rankName = "Siete";
		break;
	case SOTA:
		rankName = "Sota";
		break;
	case CABALLO:
		rankName = "Caballo";
		break;
	case REY:
		rankName = "Rey";
		break;
	default: break;
	}
	return rankName;
}



// Accessor: returns a string with the suit of the card in English 
string Card::get_english_suit() const {
	string suitName;
	switch (suit) {
	case OROS:
		suitName = "coins";
		break;
	case COPAS:
		suitName = "cups";
		break;
	case ESPADAS:
		suitName = "spades";	//opted to use spades over swords to match example logs
		break;
	case BASTOS:
		suitName = "clubs";
		break;
	default: break;
	}
	return suitName;
}

// Accessor: returns a string with the rank of the card in English 
string Card::get_english_rank() const {
	string rankName;
	switch (rank) {
	case AS:
		rankName = "Ace";
		break;
	case DOS:
		rankName = "Two";
		break;
	case TRES:
		rankName = "Three";
		break;
	case CUATRO:
		rankName = "Four";
		break;
	case CINCO:
		rankName = "Five";
		break;
	case SEIS:
		rankName = "Six";
		break;
	case SIETE:
		rankName = "Seven";
		break;
	case SOTA:
		rankName = "Jack";
		break;
	case CABALLO:
		rankName = "Knight";
		break;
	case REY:
		rankName = "King";
		break;
	default: break;
	}
	return rankName;
}



// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
	return static_cast<int>(rank) + 1;
}

// Comparison operator for cards
// Returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
	return rank < card2.rank;
}



/* *************************************************
Hand class
************************************************* */

//Constructor for Hand class, starts vector for hand
Hand::Hand() {
	Card init_card;
	hand.push_back(init_card);
	int rank = init_card.get_rank();
	if (rank > 9)
		value = 0.5;
	else
		value = rank;
}

//adds a card to current player hand, adjusting for value
void Hand::add_card() {
	Card new_card;
	double new_value;
	hand.push_back(new_card);
	int rank = new_card.get_rank();
	if (rank > 9)
		new_value = 0.5;
	else
		new_value = rank;
	value += new_value;
	cout << "New card:" << endl;
	cout << new_card.get_spanish_rank() << " de " << new_card.get_spanish_suit() << " ("
		<< new_card.get_english_rank() << " of " << new_card.get_english_suit() << ").\n" << endl;
}

//checks current hand total value
double Hand::get_value() const {
	return value;
}

//plays one hand of the game for the player, asking for inputs
void Hand::player_play() {
	bool more_cards = true;
	while (more_cards) 
	{
		char choice;

		cout << "Your cards:" << endl;
		for (int i = 0; i < hand.size(); i++)
		{
			cout << hand[i].get_spanish_rank() << " de " << hand[i].get_spanish_suit() << " ("
				<< hand[i].get_english_rank() << " of " << hand[i].get_english_suit() << ")." << endl;
		}

		if (value <= 7.5)
		{
			cout << "Your total is " << value << ". Do you want another card (y/n)? ";
			cin >> choice;
			cout << endl;
			if (choice == 'y')
				(*this).add_card();
			else
			{
				more_cards = false;
				break;
			}
		}
		else
		{
			cout << "You busted!" << endl;
			more_cards = false;
			break;
		}
	}
}

//plays one hand for the dealer, following the restrictions/rules of the game
void Hand::dealer_play() {
	cout << "Dealer's cards: " << endl;
	cout << hand[0].get_spanish_rank() << " de " << hand[0].get_spanish_suit() << " ("
		<< hand[0].get_english_rank() << " of " << hand[0].get_english_suit() << ").\n" << endl;

	while (value < 5.5)
	{
		(*this).add_card();
		cout << "Dealer's cards: " << endl;
		for (int j = 0; j < hand.size(); j++)
		{
			cout << hand[j].get_spanish_rank() << " de " << hand[j].get_spanish_suit() << " ("
				<< hand[j].get_english_rank() << " of " << hand[j].get_english_suit() << ")." << endl;
		}
		cout << "The dealer's total is " << value << ".\n" << endl;
	}
}

//create a new hand, clearing the old one and basically resetting the construction of a Hand
void Hand::new_hand() {
	hand.clear();
	Card init_card;
	hand.push_back(init_card);
	int rank = init_card.get_rank();
	if (rank > 9)
		value = 0.5;
	else
		value = rank;
}

//display each card, meant to help with logging the game
Card Hand::get_card(int i) const {
	return hand[i];
}

//displays hand size, meant to help with logging the game
int Hand::hand_size() const {
	return hand.size();
}

/* *************************************************
Player class
************************************************* */
//Default constructor, if no new starting money is given
Player::Player() {
	money = 100;
}

//Constructor for Player class, assigns initial money
Player::Player(int m) {
	money = m;
}

//return current money amount
int Player::current_money() const {
	return money;
}

//both adds and subtracts changes in money, depending on the input
//use -1 * cm to subtract
void Player::change_money(int cm) {
	money += cm;
}
