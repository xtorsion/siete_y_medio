#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
#include <cstdio>

using namespace std;
// No Global Constants
// Non member functions declarations (if any)
int winner(Hand player, Hand dealer);

// Non member functions implementations (if any)
//let 0 be a win, 1 be a loss, and 2 be a tie
int winner(Hand player, Hand dealer) {
	double player_value = player.get_value();
	double dealer_value = dealer.get_value();
	if (player_value > 7.5)
		return 1;
	else if (dealer_value > 7.5)
		return 0;
	else if (player_value <= 7.5 && dealer_value <= 7.5)
	{
		if (player_value > dealer_value)
			return 0;
		if (player_value < dealer_value)
			return 1;
		if (player_value == dealer_value)
			return 2;
	}
}

int main() {
	srand(time(0));
	Player p;
	Hand player, dealer;
	int bet, result;
	int game = 1;						//this is meant to be used to help keep track of the log
	ofstream file;

	//clear the gamelog file for a new run
	file.open("gamelog.txt", ofstream::out | ofstream::trunc);
	file.close();

	//run the game
	while (p.current_money() > 0 && p.current_money() < 1000)
	{
		cout << "You have $" << p.current_money() << ". Enter bet: $";
		cin >> bet;
		cout << endl;
		//this is to check and make sure the bet amount is valid
		while (bet > p.current_money())
		{
			cout << "You don't have enough money! Enter new bet: $";
			cin >> bet;
			cout << endl;
		}
		player.player_play();
		//this is to automatically make the player lose should they bust
		if (player.get_value() <= 7.5)
			dealer.dealer_play();

		//uses the non-member function to determine the outcome of each round
		result = winner(player, dealer);
		switch (result) {
		case 0:
			cout << "You win $" << bet << ".\n" << endl;
			p.change_money(bet);
			break;
		case 1:
			cout << "Too bad. You lose $" << bet << ".\n" << endl;
			p.change_money(-1 * bet);
			break;
		case 2:
			cout << "Nobody wins!\n" << endl;
			break;
		default:
			break;
		}

		file.open("gamelog.txt", ios_base::app);
		if (file.is_open())
		{
			file << "-------------------------------------" << endl;
			file << "Game number: " << game << endl;
			file << "Money left: $" << p.current_money() << endl;
			file << "Bet: $" << bet << endl << endl;
			file << "Your cards: " << endl;
			for (int i = 0; i < player.hand_size(); i++)
			{
				Card print = player.get_card(i);
				file << print.get_spanish_rank() << " de " << print.get_spanish_suit() << " ("
					<< print.get_english_rank() << " of " << print.get_english_suit() << ")." << endl;
			}
			file << "Your total: " << player.get_value() << ".\n" << endl;
			file << "Dealer's cards: " << endl;
			for (int i = 0; i < dealer.hand_size(); i++)
			{
				Card print = dealer.get_card(i);
				file << print.get_spanish_rank() << " de " << print.get_spanish_suit() << " ("
					<< print.get_english_rank() << " of " << print.get_english_suit() << ")." << endl;
			}
			file << "Dealer's total is " << dealer.get_value() << ".\n" << endl;
			file.close();
		}
		//reset each players hand for next game
		player.new_hand();
		dealer.new_hand();
		game++;
	}

	//Displays finishing screen upon losing all the money
	//note: p.current_money() should never go under 0
	if (p.current_money() <= 0)
	{
		cout << "You have $0. GAME OVER!" << endl << "Come back when you have more money.\n" << endl;
	}

	//Displays finishing screen upon beating the dealer, and displays current new total money
	if (p.current_money() >= 1000)
	{
		cout << "Congratulations. You beat the casino!" << endl << "You now have $" << p.current_money() << ".\n" << endl;
	}

	cout << "Bye!" << endl;

	//Pause the console so ending screen can be seen
	system("PAUSE");
	return 0;
}