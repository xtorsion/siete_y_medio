**Siete y Medio**

This repos is contains a single-player version of the card game known as ‘siete y medio’ (seven and a half), which is very similar in nature to the casino game of Blackjack (also known as 21). The goal of the game is to get cards whose total value comes the closest to 7.5 without going over it. Getting a card total over 7.5 is called “busting”.
The game will be played with Spanish baraja playing cards, which only contain 10 cards per suit. Cards 1 (Ace) to 7 will represent values of 1 to 7 respectively, and the cards representing Jack, Queen, and King (the squire, knight, and king in this case) are each 0.5 points. 
After each round, if the player ever busts, they lose their bet amount for the round. If the player wins or the dealer busts, the player wins the amount of the bet money, and if the player and dealer tie, no money is exchanged. 
Furthermore, a log of each round will be kept to keep track of the total winnings/losses of the player. 